const bodyParser = require('body-parser')
const express = require('express')
const uuid = require('uuid/v4')

const Name = require('./actions/name')

function createHandlers ({ name }) {
  function handleDashboard (req, res) {
    res.render('dashboard/templates/dashboard', { context: req.context })
  }

  function handleName (req, res) {
    const userId = req.context.currentUser.userId
    const newName = req.body.name
    const traceId = req.context.traceId

    return name(traceId, userId, newName).then(() =>
      res.render('dashboard/templates/named')
    )
  }

  return {
    handleDashboard,
    handleName
  }
}

function build ({ messageStore }) {
  const name = Name({ messageStore })
  const handlers = createHandlers({ name })

  const router = express.Router()

  router.route('/').get(handlers.handleDashboard)
  router
    .route('/name')
    .post(bodyParser.urlencoded({ extended: false }), handlers.handleName)

  return { handlers, router }
}

module.exports = build
