const uuid = require('uuid/v4')

function build ({ messageStore, clock }) {
  return async function name (traceId, userId, newName) {
    const nameCommand = {
      id: uuid(),
      type: 'Name',
      metadata: { traceId },
      data: {
        userId,
        name: newName
      }
    }

    await messageStore.write(`registration:command-${userId}`, nameCommand)
  }
}

module.exports = build
