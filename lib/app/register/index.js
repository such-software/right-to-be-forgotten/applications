const bodyParser = require('body-parser')
const express = require('express')
const uuid = require('uuid/v4')

const Register = require('./actions/register')

function createHandlers ({ register }) {
  function handleRegistrationForm (req, res) {
    const userId = uuid()

    res.render('register/templates/register', { userId })
  }

  function handleRegistrationComplete (req, res) {
    res.render('register/templates/registration-complete')
  }

  function handleRegisterUser (req, res, next) {
    const { id, email, password } = req.body

    return register(req.context.traceId, id, email, password)
      .then(() => res.redirect(301, 'register/registration-complete'))
      .catch(next)
  }

  return {
    handleRegistrationForm,
    handleRegistrationComplete,
    handleRegisterUser
  }
}

function build ({ messageStore, clock }) {
  const register = Register({ messageStore, clock })
  const handlers = createHandlers({ register })

  const router = express.Router()

  router
    .route('/')
    .get(handlers.handleRegistrationForm)
    .post(
      bodyParser.urlencoded({ extended: false }),
      handlers.handleRegisterUser
    )

  router
    .route('/registration-complete')
    .get(handlers.handleRegistrationComplete)

  return { handlers, router }
}

module.exports = build
