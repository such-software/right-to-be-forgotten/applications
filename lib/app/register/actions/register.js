const bcrypt = require('bcrypt')
const uuid = require('uuid/v4')

const SALT_ROUNDS = 10

function build ({ messageStore, clock }) {
  return async function register (traceId, userId, email, password) {
    const passwordHash = await bcrypt.hash(password, SALT_ROUNDS)

    const registerCommand = {
      id: uuid(),
      type: 'Register',
      metadata: { traceId },
      data: {
        userId,
        email,
        passwordHash,
        registeredDate: clock.iso8601Date()
      }
    }

    await messageStore.write(
      `registration:command-${userId}`,
      registerCommand,
      -1
    )
  }
}

module.exports = build
