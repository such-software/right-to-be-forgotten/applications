const uuid = require('uuid/v4')

function build ({ messageStore, clock }) {
  return function comment (traceId, videoId, commentId, commenterId, body) {
    const commentCommand = {
      id: uuid(),
      type: 'Comment',
      metadata: { traceId },
      data: {
        commentId,
        videoId,
        commenterId,
        body,
        commentTime: clock.iso8601()
      }
    }

    return messageStore.write(
      `videoComment:command-${commentId}`,
      commentCommand
    )
  }
}

module.exports = build
