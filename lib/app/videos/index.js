const bodyParser = require('body-parser')
const express = require('express')
const uuid = require('uuid/v4')

const Comment = require('./actions/comment')
const GetPublishedVideo = require('./queries/get-published-video')
const GetPublishedVideos = require('./queries/get-published-videos')
const GetVideoComments = require('./queries/get-video-comments')

function createHandlers ({
  comment,
  getPublishedVideo,
  getPublishedVideos,
  getVideoComments
}) {
  async function addComment (req, res) {
    const { body } = req.body
    const { videoId } = req.params
    const commenterId = req.context.currentUser.userId
    const commentId = uuid()

    await comment(req.context.traceId, videoId, commentId, commenterId, body)

    res.render('videos/templates/commented', { videoId })
  }

  async function index (req, res) {
    const videos = await getPublishedVideos()

    res.render('videos/templates/index', { videos })
  }

  async function video (req, res) {
    const video = await getPublishedVideo(req.params.videoId)
    const comments = await getVideoComments(req.params.videoId)

    if (!video) {
      res.render('common-templates/404')
    }

    res.render('videos/templates/video', { video, comments })
  }

  return {
    addComment,
    index,
    video
  }
}

function build ({ db, messageStore, clock }) {
  const comment = Comment({ messageStore, clock })
  const getPublishedVideo = GetPublishedVideo({ db })
  const getPublishedVideos = GetPublishedVideos({ db })
  const getVideoComments = GetVideoComments({ db })
  const handlers = createHandlers({
    comment,
    getPublishedVideo,
    getPublishedVideos,
    getVideoComments
  })

  const router = express.Router()

  router.route('/').get(handlers.index)
  router.route('/:videoId').get(handlers.video)
  router
    .route('/:videoId/comments')
    .post(bodyParser.urlencoded({ extended: false }), handlers.addComment)

  return { handlers, router }
}

module.exports = build
