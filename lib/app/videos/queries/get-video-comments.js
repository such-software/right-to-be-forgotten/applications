const camelCaseKeys = require('camelcase-keys')

function build ({ db }) {
  return function getVideoComments (videoId) {
    return db
      .then(client =>
        client('comments')
          .leftJoin('users', 'comments.commenter_id', '=', 'users.user_id')
          .select(
            'comments.comment_id as comment_id',
            'users.name as name',
            'comments.body as body',
            'comments.comment_time as comment_time'
          )
          .orderBy('comment_time', 'DESC')
      )
      .then(camelCaseKeys)
  }
}

module.exports = build
