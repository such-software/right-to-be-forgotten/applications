const camelCaseKeys = require('camelcase-keys')

function build ({ db }) {
  return function getPublishedVideo (videoId) {
    return db
      .then(client =>
        client('videos')
          .where({ video_id: videoId })
          .whereNotNull('published_date')
          .limit(1)
      )
      .then(camelCaseKeys)
      .then(rows => rows[0])
  }
}

module.exports = build
