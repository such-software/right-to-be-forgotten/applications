const camelCaseKeys = require('camelcase-keys')

function build ({ db }) {
  return function getPublishedVideos () {
    return db
      .then(client => client('videos').whereNotNull('published_date'))
      .then(camelCaseKeys)
  }
}

module.exports = build
