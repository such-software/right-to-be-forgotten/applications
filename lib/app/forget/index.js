const bodyParser = require('body-parser')
const express = require('express')
const uuid = require('uuid/v4')

const Forget = require('./actions/forget')
const GetUsers = require('./queries/get-users')

function createHandlers ({ forget, getUsers }) {
  async function handleIndex (req, res) {
    const users = await getUsers()

    res.render('forget/templates/index', { users })
  }

  async function handleForget (req, res, next) {
    const { id } = req.body

    await forget(req.context.traceId, id)

    res.render('forget/templates/forgot')
  }

  return {
    handleIndex,
    handleForget
  }
}

function build ({ db, messageStore, clock }) {
  const forget = Forget({ messageStore, clock })
  const getUsers = GetUsers({ db })
  const handlers = createHandlers({ forget, getUsers })

  const router = express.Router()

  router
    .route('/')
    .get(handlers.handleIndex)
    .post(bodyParser.urlencoded({ extended: false }), handlers.handleForget)

  return { handlers, router }
}

module.exports = build
