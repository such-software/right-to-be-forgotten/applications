const uuid = require('uuid/v4')

function build ({ messageStore, clock }) {
  return function forget (traceId, userId) {
    const forgetCommand = {
      id: uuid(),
      type: 'Gdpr',
      metadata: { traceId },
      data: {
        userId,
        effectiveTime: clock.iso8601()
      }
    }

    return messageStore.write(`registration:command-${userId}`, forgetCommand)
  }
}

module.exports = build
