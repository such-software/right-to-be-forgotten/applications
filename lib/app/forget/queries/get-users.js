const camelCaseKeys = require('camelcase-keys')

function build ({ db }) {
  return function getUsers () {
    return db.then(client => client('users').then(camelCaseKeys))
  }
}

module.exports = build
