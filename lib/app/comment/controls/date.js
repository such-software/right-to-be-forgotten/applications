module.exports = {
  example () {
    return this.timeString
  },

  raw () {
    return new Date(this.timeString)
  },

  timeString: '2020-01-01T00:00:00'
}
