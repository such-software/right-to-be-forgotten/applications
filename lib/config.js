/*
The system makes heavy use of dependency injection.  This file wires up all the
dependencies, making use of the environment.
*/

const KnexClient = require('./knex-client')
const PostgresClient = require('./postgres-client')
const MessageStore = require('./message-store')
const RegisterApp = require('./app/register')
const ForgetApp = require('./app/forget')
const DashboardApp = require('./app/dashboard')
const VideosApp = require('./app/videos')
const LoadCurrentUser = require('./app/express/load-current-user')

const clock = require('./clock')

function createConfig ({ env }) {
  const knexClient = KnexClient({
    connectionString: env.databaseUrl
  })
  const postgresClient = PostgresClient({
    connectionString: env.messageStoreConnectionString
  })
  const messageStore = MessageStore({ db: postgresClient })
  const registerApp = RegisterApp({ messageStore, clock })
  const forgetApp = ForgetApp({ db: knexClient, messageStore, clock })
  const dashboardApp = DashboardApp({ messageStore })
  const videosApp = VideosApp({ clock, db: knexClient, messageStore })
  const loadCurrentUser = LoadCurrentUser({ db: knexClient })

  return {
    env,
    db: knexClient,
    messageStore,
    registerApp,
    forgetApp,
    dashboardApp,
    videosApp,
    loadCurrentUser
  }
}

module.exports = createConfig
