// eslint-disable-next-line import/no-extraneous-dependencies
const gdprAggregators = require('@suchsoftware/gdpr-demo-aggregators')
const Bluebird = require('bluebird')
const test = require('blue-tape')
// eslint-disable-next-line import/no-extraneous-dependencies

const { app, config } = require('../lib')

test.onFinish(() => {
  config.messageStore.stop()
  bootstrappedDb.then(client => client.destroy())
})

/* eslint-disable no-console */
process.on('unhandledRejection', err => {
  console.error('Uh-oh. Unhandled Rejection')
  console.error(err)

  process.exit(1)
})
/* eslint-enable no-console */

function reset () {
  const tablesToWipe = []

  return Bluebird.each(tablesToWipe, table =>
    config.db.then(client => client(table).del())
  )
}

const aggregatorMigrationSettings = {
  tableName: 'aggregator_migrations',
  migrationSource: gdprAggregators.migrations
}

const bootstrappedDb = config.db.tap(client =>
  client.migrate.latest(aggregatorMigrationSettings)
)

module.exports = {
  app,
  messageStore: config.messageStore,
  db: bootstrappedDb,
  reset
}
