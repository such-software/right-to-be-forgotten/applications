const test = require('blue-tape')
const uuid = require('uuid/v4')

const { messageStore } = require('../../automated-init')
const Name = require('../../../lib/app/dashboard/actions/name')

test('It writes a Name command', t => {
  const traceId = uuid()
  const userId = uuid()
  const newName = 'Ethan Garofolo'

  const name = Name({ messageStore })

  return name(traceId, userId, newName)
    .then(() => messageStore.read(`registration:command-${userId}`))
    .then(messages => {
      t.equal(messages.length, 1, '1 written')

      const [nameCommand] = messages

      t.equal(nameCommand.type, 'Name', 'Is a Name command')
      t.equal(nameCommand.data.userId, userId, 'Correct userId')
      t.equal(nameCommand.data.name, newName, 'Correct name')
      t.equal(nameCommand.metadata.traceId, traceId, 'Correct traceId')
    })
})
