const test = require('blue-tape')
const uuid = require('uuid/v4')

const { messageStore } = require('../../automated-init')
const Forget = require('../../../lib/app/forget/actions/forget')

const DateControls = require('../../../lib/app/forget/controls/date')

const clock = { iso8601Date: () => DateControls.example() }

test('It writes a Register command', t => {
  const traceId = uuid()
  const userId = uuid()
  const effectiveDate = clock.iso8601Date()

  const forget = Forget({ messageStore, clock })

  return forget(traceId, userId).then(() =>
    messageStore.read(`registration:command-${userId}`).then(messages => {
      t.equal(messages.length, 1, '1 written')

      const [forgetCommand] = messages

      t.equal(forgetCommand.type, 'Gdpr', 'It is a Gdpr command')
      t.equal(forgetCommand.data.userId, userId, 'Correct userId')
      t.equal(forgetCommand.data.effectiveDate, effectiveDate, 'Correct date')
      t.equal(forgetCommand.metadata.traceId, traceId, 'Correct traceId')
    })
  )
})
