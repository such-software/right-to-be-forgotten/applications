const test = require('blue-tape')
const uuid = require('uuid/v4')

const { messageStore } = require('../../automated-init')
const Register = require('../../../lib/app/register/actions/register')

const DateControls = require('../../../lib/app/register/controls/date')

const clock = { iso8601Date: () => DateControls.example() }

test('It writes a Register command', t => {
  const traceId = uuid()
  const userId = uuid()
  const email = 'ethan@suchsoftware.com'
  const password = '$3cur3p@$$word'
  const registeredDate = clock.iso8601Date()

  const register = Register({ messageStore, clock })

  return register(traceId, userId, email, password)
    .then(() => messageStore.read(`registration:command-${userId}`))
    .then(messages => {
      t.equal(messages.length, 1, '1 written')

      const [registerCommand] = messages

      t.equal(registerCommand.type, 'Register', 'Is a Register command')
      t.equal(registerCommand.data.userId, userId, 'Correct userId')
      t.equal(registerCommand.data.email, email, 'Correct email')
      t.assert(registerCommand.data.passwordHash, 'There is a password Hash')
      t.equal(
        registerCommand.data.registeredDate,
        registeredDate,
        'Correct date'
      )
      t.equal(registerCommand.metadata.traceId, traceId, 'Correct traceId')
    })
})
