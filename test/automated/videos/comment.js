const test = require('blue-tape')
const uuid = require('uuid/v4')

const { messageStore } = require('../../automated-init')
const Comment = require('../../../lib/app/videos/actions/comment')

const DateControls = require('../../../lib/app/videos/controls/date')

const clock = { iso8601Date: () => DateControls.example() }

test('It writes a Comment command', t => {
  const traceId = uuid()
  const videoId = uuid()
  const commenterId = uuid()
  const commentId = uuid()
  const body = 'comment'

  const commentTime = clock.iso8601Date()

  const comment = Comment({ messageStore, clock })

  return comment(traceId, videoId, commentId, commenterId, body).then(() =>
    messageStore.read(`videoComment:command-${commentId}`).then(messages => {
      t.equal(messages.length, 1, '1 written')

      const [commentCommand] = messages

      t.equal(commentCommand.type, 'Comment', 'It is a Comment command')
      t.equal(commentCommand.data.videoId, videoId, 'Correct videoId')
      t.equal(
        commentCommand.data.commenterId,
        commenterId,
        'Correct commenterId'
      )
      t.equal(commentCommand.data.commentId, commentId, 'Correct commentId')
      t.equal(commentCommand.data.commentTime, commentTime, 'Correct time')
      t.equal(commentCommand.data.body, body, 'Correct body')
      t.equal(commentCommand.metadata.traceId, traceId, 'Correct traceId')
    })
  )
})
